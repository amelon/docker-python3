# docker-python3
# Source: https://bitbucket.org/amelon/docker-python3

## Requirements for Development on macOS
* macOS 14.4 (Big Sur) or higher
* MacBook Pro or higher with 16GB RAM
* Docker Desktop for Mac Installation
    * Download package at https://hub.docker.com/editions/community/docker-ce-desktop-mac and install it.

## Project Build and Deployment
The steps below can be used to build and deploy the Python3 container.

    $ docker-compose build

Once the build completes and is successful, bring up the container.

    $ docker-compose up -d

Verify that the container is up and running.

    $ docker-compose ps
    NAME            COMMAND        SERVICE    STATUS         PORTS
    -------------------------------------------------------------------
    python3-001     "bash"         python3    running

Enter the container's Bash shell.

    $ docker-compose exec python3 bash

Check the environment. It should look something like the following.

    root@fa9458e1a0e4:/source# which python
    /usr/bin/python

    root@fa9458e1a0e4:/source# python --version
    Python 3.8.10

    root@fa9458e1a0e4:/source# which pip
    /usr/bin/pip

If everything checks out, you are now ready to run your Python script. Map your source code to the project's `source/` directory.

