FROM ubuntu:20.04
MAINTAINER Vui Le "amelon@gmail.com"

RUN apt-get update \
    &&  DEBIAN_FRONTEND=noninteractive apt-get install -y \
              vim \
              build-essential \
              openssl \
              libffi-dev \
              libssl-dev \
              libyaml-dev \
              sshpass \
              language-pack-en-base \
              software-properties-common \
              python3 \
              python3-dev \
              python3-pip \
              python3-venv \
              python-is-python3 \
              curl \
              sudo \
              jq \
              tzdata \
              iputils-ping \
              dnsutils \
              rsync \
              rlwrap

RUN mv /etc/localtime /etc/localtime.old \
    && ln -s /usr/share/zoneinfo/America/Los_Angeles /etc/localtime

RUN mkdir -p /configs /source
COPY configs /configs

RUN pip3 install --upgrade --trusted-host pypi.python.org -r /configs/requirements.txt

WORKDIR /source
USER root

